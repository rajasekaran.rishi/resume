# C'mon, you gotta have a Makefile if
# you're constantly having to generate a document

default: clean
	xelatex rishi-resume
	evince rishi-resume.pdf & # I like to view the PDF ASAP

intermediates=rishi-resume.log rishi-resume.out rishi-resume.aux

clean_intermediates: 
	rm $(intermediates) -f

clean: clean_intermediates
	rm rishi-resume.pdf -f

