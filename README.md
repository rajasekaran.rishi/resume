# Rishi's Resume

Based on Deedy's resume


## Creating the PDF
You'll need a bunch of LaTeX stuff, so your best bet is to install `texlive-full`.

A simple `Makefile` has been provided (along with a dependency on evince). 
Therefore just simply run `make` to generate the PDF and `make clean` to clean up.
